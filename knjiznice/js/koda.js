/* global $ btoa */

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";
  var ime = "";
  var priimek = "";
  var datumRojstva = "";
  var spol = "";
  
  switch (stPacienta) {
    case 1:
      // atlet
      ime = "Peter";
      priimek = "Klepec";
      datumRojstva = "1997-10-06T01:00Z";
      spol = "MALE";
      break;
    case 2:
      // child k se rase
      ime = "Živa";
      priimek = "Groza";
      datumRojstva = "2004-04-05T03:00Z";
      spol = "FEMALE";
      break;
    case 3:
      // zavaljen starec
      ime = "Danny";
      priimek = "DeVito";
      datumRojstva = "1944-11-17T04:20Z";
      spol = "MALE";
      break;
    default:
      console.log("Error - Invalid input value for function generirajPodatke(stPacienta) @ /knjiznice/js/koda.js");
      break;
  }

  $.ajaxSetup({
    headers: {
      "Authorization": getAuthorization()
    }
  });
  $.ajax({
    url: baseUrl + "/ehr",
    type: "POST",
    success: function(data){
      console.log("Created new patient!");
      ehrId = data.ehrId;
      console.log("ehrID for " + ime + ": " + ehrId);
      if (stPacienta == 1) {
        $("#gen1").attr("value", ehrId);
      } else if (stPacienta == 2) {
        $("#gen2").attr("value", ehrId);
      } else if (stPacienta == 3) {
        $("#gen3").attr("value", ehrId);
      }
      
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: datumRojstva,
        gender: spol,
        partyAdditionalInfo: [{
          key: "ehrId",
          value: ehrId
        }]
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party) {
          if (party.action == 'CREATE') {
            console.log("Creating new measurements...");
            switch (stPacienta) {
              case 1:
                createMeasurement(ehrId, "80.0", "185.5", "36.4", "2009-04-20T10:40");
                createMeasurement(ehrId, "81.1", "185.5", "35.9", "2010-04-21T10:39");
                createMeasurement(ehrId, "80.9", "185.5", "36.6", "2011-04-19T10:42");
                createMeasurement(ehrId, "80.4", "185.5", "36.1", "2012-04-24T09:50");
                createMeasurement(ehrId, "81.6", "185.5", "36.2", "2013-04-22T10:35");
                break;
              case 2:
                createMeasurement(ehrId, "36.9", "144.0", "36.1", "2015-04-20T10:40");
                createMeasurement(ehrId, "41.5", "149.8", "36.0", "2016-04-20T10:43");
                createMeasurement(ehrId, "45.8", "156.7", "36.0", "2017-04-20T10:38");
                createMeasurement(ehrId, "47.6", "158.7", "36.3", "2018-04-20T09:55");
                createMeasurement(ehrId, "52.1", "159.7", "37.8", "2019-04-20T10:20");
                break;
              case 3:
                createMeasurement(ehrId, "85.0", "175.5", "36.5", "2009-04-20T10:40");
                createMeasurement(ehrId, "87.4", "175.5", "36.2", "2010-04-21T10:41");
                createMeasurement(ehrId, "90.8", "175.4", "36.0", "2011-04-25T10:34");
                createMeasurement(ehrId, "95.5", "175.4", "36.4", "2012-04-15T09:54");
                createMeasurement(ehrId, "99.6", "175.2", "36.0", "2013-04-18T09:44");
                break;
            }
            $("#datagen").html("<div class='alert alert-info text-center'>Podatki generirani!</div>");
          }
          console.log("Successfully created a new EHR record with measurements.");
        },
        error: function(err) {
          console.log("Error while entering new data!");
          console.log(JSON.parse(err.responseText));
        }
      });
    }
  });
  return ehrId;
}

function createMeasurement(ehrId, weight, height, temp, dateTime) {
  $.ajaxSetup({
    headers: {
      "Authorization": getAuthorization()
    }
  });
  var meritve = {
    "ctx/language": "en",
    "ctx/territory": "SI",
    "ctx/time": dateTime,
    "vital_signs/height_length/any_event/body_height_length": height,
    "vital_signs/body_weight/any_event/body_weight": weight,
    "vital_signs/body_temperature/any_event/temperature|magnitude": temp,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
  };
  var params = {
    ehrId: ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT'
  };
  $.ajax({
    url: baseUrl + "/composition?" + $.param(params),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(meritve),
    success: function (res) {
      console.log("Uspesno ustvarjena meritev");
    },
    error: function(err) {
      console.log("Error while creating a measurement!");
      console.log(err.responseText);
    }
  });
}

// Ob kliku na gumb "Generiraj podatke" se izvede ta funkcija
function onClickDataGen() {
  console.log("Generating new data...");
  for (var i=1; i<4; i++) {
    generirajPodatke(i);
  }
}

// prikaze podatke in meritve za podani ehrId iz polja za vnos ehrId
function prikaziEHR() {
  var ehrId = $("#preberiEHRid").val();
  if (ehrId == "" || !ehrId) {
    $("#ehrMsg").html("<div class='alert alert-danger text-center'>Vnesite EHRID ali izberite pacienta!</div>");
  } else {
    $("#ehrMsg").html("");
  }
  
  $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (data) {
      var party = data.party;
      console.log(party),
      $("#imePriimek").val(party.firstNames + " " + party.lastNames);
      $("#gender").val(party.gender);
      $("#birthDate").val(party.dateOfBirth);
      $.ajax({
        url: baseUrl +"/view/" + ehrId +"/body_temperature",
        type: 'GET',
        headers: {
          "Authorization": getAuthorization()
        },
        success: function (res) {
          for (var i in res) {
            $('#datumMeritve').append("<option id='vit" + i + "' value='" + res[i].temperature + "'>" + res[i].time + "</option");
          }
          $.ajax({
            url: baseUrl +"/view/" + ehrId +"/height",
            type: 'GET',
            headers: {
              "Authorization": getAuthorization()
            },
            success: function (res) {
              for (var i in res) {
                $("#vit" + i).val(function(){
                  return this.value + "|" + res[i].height;
                });
              }
              $.ajax({
                url: baseUrl +"/view/" + ehrId +"/weight",
                type: 'GET',
                headers: {
                  "Authorization": getAuthorization()
                },
                success: function (res) {
                  for (var i in res) {
                    $("#vit" + i).val(function(){
                      return this.value + "|" + res[i].weight;
                    });
                  }
                },
                error: function(err) {
                  console.log(err.responseText);
                  $("#ehrMsg").html("<div class='alert alert-danger text-center'>Napaka pri prikazovanju podatkov</div>");
                }
              });
            },
            error: function(err) {
              console.log(err.responseText);
              $("#ehrMsg").html("<div class='alert alert-danger text-center'>Napaka pri prikazovanju podatkov</div>");
            }
          });
        },
        error: function(err) {
          console.log(err.responseText);
          $("#ehrMsg").html("<div class='alert alert-danger text-center'>Napaka pri prikazovanju podatkov</div>");
        }
      });
    },
    error: function(err) {
      console.log(err.responseText);
      $("#ehrMsg").html("<div class='alert alert-danger text-center'>Napačen EHRID</div>");
    }
  });
}

$(document).ready(function() {
  
  $("#deathButton").click(function() {
    console.log("Kliknili ste na death button! Beware!")
    var gender = $("#gender").val().toLowerCase();
    console.log("gender: " + gender);
    var birthDate = $("#birthDate").val();
    console.log("birthday: " + birthDate);
    if ((gender == "") || (birthDate == "")) {
      console.log("Empty data!");
      $("#deathMsg").html("<div class='alert alert-danger text-center'>Najprej izberite pacienta!</div>");
      return;
    }
    
    var meseci = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var leto = birthDate.substring(0, 4); console.log(leto);
    var dan = birthDate.substring(8, 10); console.log(dan);
    var stMeseca = parseInt(birthDate.substring(5, 7)); console.log(stMeseca);
    var mesec = meseci[stMeseca - 1]; console.log(mesec);
    
    $.ajax({
      headers: {
        "X-RapidAPI-Host": "life-left.p.rapidapi.com",
        "X-RapidAPI-Key": "fe92070e1dmsheae361f20365e72p106a45jsnaa0e377e7860"
      },
      type: "GET",
      url: "https://life-left.p.rapidapi.com/time-left?gender=" + gender + "&birth=" + dan + "+" + mesec + "+" + leto,
      success: function(res) {
        console.log(res.status);
        console.log(res.headers);
        console.log(res.body);
      },
      error: function(err) {
        console.log("Error while getting death message!");
        console.log(err.responseText);
        $("#deathMsg").html("<div class='alert alert-info text-center'>Do smrti imate na voljo še 20 dni brezskrbnega življenja :D</div>");
      }
    });
  });
  
  $("#prikaziEHRid").change(function() {
    $("#preberiEHRid").val($(this).val());
    console.log("prikazan bo id: " + $(this).val());
    $('#datumMeritve')
    .find('option')
    .remove()
    .end()
    .append('<option value=""></option>');
    
    $("#imePriimek").val("");
    $("#gender").val("");
    $("#birthDate").val("");
    
    $("#temperatura").val("");
    $("#visina").val("");
    $("#masa").val("");
  });
  
  $("#datumMeritve").change(function() {
    var podatki = $(this).val().split("|");
    $("#ehrMsg").html("");
    $("#temperatura").val(podatki[0]);
    $("#visina").val(podatki[1]);
    $("#masa").val(podatki[2]);
  });
  
});