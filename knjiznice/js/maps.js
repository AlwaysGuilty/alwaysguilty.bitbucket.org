/* global L, distance, $ */


var mapa;
var obmocje;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 12
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Objekt oblačka markerja
  var popup = L.popup();

  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
  }

  mapa.on('click', obKlikuNaMapo);
  
  var bolnisnice = {};
  
  $.ajax({
    url: "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json",
    type: "GET",
    dataType: "text",
    success: function(res) {
      console.log(res);
      bolnisnice = JSON.parse(res);
      for (var i=0; i<bolnisnice.features.length; i++) {
        if (bolnisnice.features[i].properties["addr:city"] == "Ljubljana") {
          //L.geoJSON(bolnisnice.features[i]).addTo(mapa);
          var bolnica = bolnisnice.features[i];
          /*
          for (var i=0; i<bolnica.geometry.coordinates[0].length; i++) {
            for (var j=0; j<bolnica.geometry.coordinates[0][i].length; j++) {
              bolnica.geometry.coordinates[0][i][j] = [bolnica.geometry.coordinates[0][i][j][1], bolnica.geometry.coordinates[0][i][j][0]];
            }
          }*/
          var coords = bolnica.geometry.coordinates;
          var polignom = L.polygon(coords);
          polignom.addTo(mapa);
        }
      }
    },
    error: function(err) {
      console.log("Error while parsing data!");
      console.log(err.responseText);
    }
  });
  
});

/**
 * Preveri ali izbrano oznako na podanih GPS koordinatah izrišemo
 * na zemljevid glede uporabniško določeno vrednost radij, ki
 * predstavlja razdaljo od FRI.
 * 
 * Če radij ni določen, je enak 0 oz. je večji od razdalje izbrane
 * oznake od FRI, potem oznako izrišemo, sicer ne.
 * 
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 */
function prikaziOznako(lng, lat) {
  var radij = vrniRadij();
  if (radij == 0)
    return true;
  else if (distance(lat, lng, FRI_LAT, FRI_LNG, "K") >= radij) 
    return false;
  else
    return true;
}


/**
 * Na zemljevidu nariši rdeč krog z transparentnim rdečim polnilom
 * s središčem na lokaciji FRI in radijem. Območje se izriše 
 * le, če je na strani izbrana vrednost "Prikaz radija".
 */
function prikaziObmocje() {
  if (document.getElementById("idRadij").checked) {
    if (obmocje != null) mapa.removeLayer(obmocje);
    obmocje = L.circle([FRI_LAT, FRI_LNG], {
      color: 'red',
      fillColor: 'red',
      fillOpacity: 0.10,
      radius: vrniRadij() * 1000
    }).addTo(mapa);
  } else if (obmocje != null) {
    mapa.removeLayer(obmocje);
  }
}


/**
 * Vrni celoštevilsko vrednost radija, ki ga uporabnik vnese v 
 * vnosno polje. Če uporabnik vnese neveljavne podatke, je
 * privzeta vrednost radija 0.
 */
function vrniRadij() {
  var vrednost = document.getElementById("radij");
  if (vrednost == null) {
    vrednost = 0;
  } else {
    vrednost = parseInt(vrednost.value, 10);
    vrednost = isNaN(vrednost) ? 0 : vrednost;
  }
  return vrednost;
}
